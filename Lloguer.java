import java.util.Date;
import java.util.Vector;

@SuppressWarnings("unused")
public class Lloguer {
    private Date data;
    private int dies;
    private Vehicle vehicle;
    private int QUANTITAT_VEHICLE_BASIC_ENTRE_1_I_3_DIES = 3;
    private int NOMBRE_DIES_INICIALS_AMB_MATEIX_PREU_BASIC = 3;
    private int QUANTITAT_VEHICLE_GENERAL_ENTRE_1_I_2_DIES = 4;
    private int NOMBRE_DIES_INICIALS_AMB_MATEIX_PREU_GENERAL = 2;
    private int QUANTITAT_VEHICLE_LUXE = 6;

    public Lloguer(Date date, int dies, Vehicle vehicle) {
        this.data = date;
        this.dies = dies;
        this.vehicle = vehicle;
    }

    public Date getData()     { return data; }
    public int getDies()     { return dies; }

    public void setData(Date data) { this.data = data; }
    public void setDies(int dies) { this.dies = dies; }

	public Vehicle getVehicle() {
		return vehicle;

	}
	
	public double preuLloguer() {
    	int preu = 0;
    	switch (vehicle.getCategoria()) {
	        case Vehicle.BASIC:
	        	preu += QUANTITAT_VEHICLE_BASIC_ENTRE_1_I_3_DIES;
	            if (dies > NOMBRE_DIES_INICIALS_AMB_MATEIX_PREU_BASIC) {
	            	preu += (dies - NOMBRE_DIES_INICIALS_AMB_MATEIX_PREU_BASIC) * 1.5;
	            }
	            break;
	        case Vehicle.GENERAL:
	        	preu += QUANTITAT_VEHICLE_GENERAL_ENTRE_1_I_2_DIES;
	            if (dies > NOMBRE_DIES_INICIALS_AMB_MATEIX_PREU_GENERAL) {
	            	preu += (dies - NOMBRE_DIES_INICIALS_AMB_MATEIX_PREU_GENERAL) * 2.5;
	            }
	            break;
	        case Vehicle.LUXE:
	        	preu += dies * QUANTITAT_VEHICLE_LUXE;
	            break;
    	}
    	return preu;
	}

	
	public int bonificacions() {
		int bonificacions = 0;
		
		// afegeix lloguers freqüents
        bonificacions ++;

        // afegeix bonificació per dos dies de lloguer de Luxe
        if (getVehicle().getCategoria() == Vehicle.LUXE &&
                getDies()>1 ) {
            bonificacions ++;
        }
		return bonificacions;
	}
}
