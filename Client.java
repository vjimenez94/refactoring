import java.util.Vector;

public class Client {
    private String nif;
    private String nom;
    private String telefon;
    private Vector<Lloguer> lloguers;
    private static final double EUROS_PER_UNITAT_DE_COST = 30;

    public Client(String nif, String nom, String telefon) {
        this.nif = nif;
        this.nom = nom;
        this.telefon = telefon;
        this.lloguers = new Vector<Lloguer>();
    }

    public String getNif()     { return nif;     }
    public String getNom()     { return nom;     }
    public String getTelefon() { return telefon; }
    
    public void setNif(String nif) { this.nif = nif; }
    public void setNom(String nom) { this.nom = nom; }
    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }
    public void afegeix(Lloguer lloguer) {
        if (! lloguers.contains(lloguer) ) {
            lloguers.add(lloguer);
        }
    }
    
    
    public Vector<Lloguer> getLloguers(){
    	return lloguers;
    }
    
    
    public void elimina(Lloguer lloguer) {
        if (lloguers.contains(lloguer) ) {
            lloguers.remove(lloguer);
        }
    }
    
    public boolean conte(Lloguer lloguer) {
        return lloguers.contains(lloguer);
    }
    
    public double importTotal(){
		int total = 0;
		for (Lloguer lloguer: lloguers) {
			total += lloguer.preuLloguer() * EUROS_PER_UNITAT_DE_COST;
		}
		return total;
	}
    
    public int bonificacionsTotals(){
		int bonificacions = 0;
		for (Lloguer lloguer: lloguers) {
			bonificacions += lloguer.bonificacions();
		}
		return bonificacions;
	}
    
    //capsalera
    public String composaCapsalera(){
    	String resultat = "Informe de lloguers del client " +
                getNom() +
                " (" + getNif() + ")\n";
    	return resultat;
	}
    
    //detall
    public String composaDetall(){
		String resultat = "";
		for (Lloguer lloguer: lloguers) {
            // composa els resultats d'aquest lloguer
            resultat += "\t" +
                lloguer.getVehicle().getMarca() +
                " " +
                lloguer.getVehicle().getModel() + ": " +
                (lloguer.preuLloguer() * EUROS_PER_UNITAT_DE_COST) + "€" + "\n";
        }
		return resultat;
	}
    
    //peu
    public String composaPeu() {
		String resultat = "";
		resultat += "Import a pagar: " + importTotal() + "€\n" +
	            "Punts guanyats: " + bonificacionsTotals() + "\n";
	        return resultat;
    }
    
    public String informe() {
        return composaCapsalera() +
               composaDetall() +
               composaPeu();
    }
    
    public String informeHTML(){
    	String resultat = "";
    	
    	resultat = "<h1>Informe de lloguers</h1>\n";
    	resultat += "<p>Informe de lloguers del client <em>" + getNom() + "</em>";
    	resultat += "(<strong>" + getNif() + "</strong>)</p>\n";
    	resultat += "<table>\n";
		resultat += "<tr><td><strong>Marca</strong></td>";
		resultat += "<td><strong>Model</strong></td>";
		resultat += "<td><strong>Import</strong></td></tr>\n";
		for (Lloguer lloguer: lloguers) {
			resultat += "\t<tr><td>" +
			lloguer.getVehicle().getMarca() +"</td><td>" +
			lloguer.getVehicle().getModel() + "</td><td>" +
			(lloguer.preuLloguer() * EUROS_PER_UNITAT_DE_COST) + "€</td></tr>";
		}
		resultat += "\n</table>";
		resultat += "\n<p>Import a pagar: <em>" + importTotal() + "€</em></p>\n" +
		"<p>Punts guanyats: <em>" + bonificacionsTotals() + "</em></p>\n";
    	
    	return resultat;
    }

}
