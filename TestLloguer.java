import org.junit.*;
import static org.junit.Assert.*;    // importa una classe static

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("unused")
public class TestLloguer {
   
	@Test
    public void UnDiaDeLloguerBasic(){
		String informeComprova = "Informe de lloguers del client Victor (41007341R)\n";
		informeComprova= informeComprova + "\tSeat Leon: 90.0€\n";
		informeComprova= informeComprova + "Import a pagar: 90.0€\n";
		informeComprova= informeComprova + "Punts guanyats: 1";
		
		Client client = new Client("41007341R","Victor","691308569");
		Vehicle vehicleBasic = new Vehicle("Leon", "Seat", Vehicle.BASIC);
		SimpleDateFormat format = new SimpleDateFormat("d/M/yyyy");
		try{
		    Date data = format.parse("2/5/2016");
		    Lloguer lloguerBasic = new Lloguer(data, 1, vehicleBasic);
			client.afegeix(lloguerBasic);
		    assertEquals(informeComprova, client.informe());
		} catch ( Exception e){
	    	System.out.println("Error al crear el lloguer");
	    }
	}
	
	@Test
    public void CuatreDiesDeLloguerBasic(){
		String informeComprova = "Informe de lloguers del client Victor (41007341R)\n";
		informeComprova= informeComprova + "\tSeat Leon: 120.0€\n";
		informeComprova= informeComprova + "Import a pagar: 120.0€\n";
		informeComprova= informeComprova + "Punts guanyats: 1";
		
		Client client = new Client("41007341R","Victor","691308569");
		Vehicle vehicleBasic = new Vehicle("Leon", "Seat", Vehicle.BASIC);
		SimpleDateFormat format = new SimpleDateFormat("d/M/yyyy");
		try{
		    Date data = format.parse("2/5/2016");
		    Lloguer lloguerBasic = new Lloguer(data, 4, vehicleBasic);
			client.afegeix(lloguerBasic);
		    assertEquals(informeComprova, client.informe());
		} catch ( Exception e){
	    	System.out.println("Error al crear el lloguer");
	    }
	}
	
	@Test
    public void UnDiaDeLloguerGeneral(){
		String informeComprova = "Informe de lloguers del client Victor (41007341R)\n";
		informeComprova= informeComprova + "\tMercedes Benz: 120.0€\n";
		informeComprova= informeComprova + "Import a pagar: 120.0€\n";
		informeComprova= informeComprova + "Punts guanyats: 1";
		
		Client client = new Client("41007341R","Victor","691308569");
		Vehicle vehicleGeneral = new Vehicle("Mercedes", "Benz", Vehicle.GENERAL);
		SimpleDateFormat format = new SimpleDateFormat("d/M/yyyy");
		try{
		    Date data = format.parse("2/5/2016");
		    Lloguer lloguerGeneral = new Lloguer(data, 1, vehicleGeneral);
			client.afegeix(lloguerGeneral);
		    assertEquals(informeComprova, client.informe());
		} catch ( Exception e){
	    	System.out.println("Error al crear el lloguer");
	    }
	}
	
	@Test
    public void SetDiesDeLloguerGeneral(){
		String informeComprova = "Informe de lloguers del client Victor (41007341R)\n";
		informeComprova= informeComprova + "\tMercedes Benz: 480.0€\n";
		informeComprova= informeComprova + "Import a pagar: 480.0€\n";
		informeComprova= informeComprova + "Punts guanyats: 1";
		
		Client client = new Client("41007341R","Victor","691308569");
		Vehicle vehicleGeneral = new Vehicle("Mercedes", "Benz", Vehicle.GENERAL);
		SimpleDateFormat format = new SimpleDateFormat("d/M/yyyy");
		try{
		    Date data = format.parse("2/5/2016");
		    Lloguer lloguerGeneral = new Lloguer(data, 7, vehicleGeneral);
			client.afegeix(lloguerGeneral);
		    assertEquals(informeComprova, client.informe());
		} catch ( Exception e){
	    	System.out.println("Error al crear el lloguer");
	    }
	}
	
	@Test
    public void UnDiaDeLloguerLuxe(){
		String informeComprova = "Informe de lloguers del client Victor (41007341R)\n";
		informeComprova= informeComprova + "\tFord Mustang: 180.0€\n";
		informeComprova= informeComprova + "Import a pagar: 180.0€\n";
		informeComprova= informeComprova + "Punts guanyats: 1";
		
		Client client = new Client("41007341R","Victor","691308569");
		Vehicle vehicleLuxe = new Vehicle("Ford", "Mustang", Vehicle.LUXE);
		SimpleDateFormat format = new SimpleDateFormat("d/M/yyyy");
		try{
		    Date data = format.parse("2/5/2016");
		    Lloguer lloguerLuxe = new Lloguer(data, 1, vehicleLuxe);
			client.afegeix(lloguerLuxe);
		    assertEquals(informeComprova, client.informe());
		} catch ( Exception e){
	    	System.out.println("Error al crear el lloguer");
	    }
	}
	
	@Test
    public void SetDiesDeLloguerLuxe(){
		String informeComprova = "Informe de lloguers del client Victor (41007341R)\n";
		informeComprova= informeComprova + "\tFord Mustang: 1260.0€\n";
		informeComprova= informeComprova + "Import a pagar: 1260.0€\n";
		informeComprova= informeComprova + "Punts guanyats: 2";
		
		Client client = new Client("41007341R","Victor","691308569");
		Vehicle vehicleLuxe = new Vehicle("Ford", "Mustang", Vehicle.LUXE);
		SimpleDateFormat format = new SimpleDateFormat("d/M/yyyy");
		try{
		    Date data = format.parse("2/5/2016");
		    Lloguer lloguerLuxe = new Lloguer(data, 7, vehicleLuxe);
			client.afegeix(lloguerLuxe);
		    assertEquals(informeComprova, client.informe());
		} catch ( Exception e){
	    	System.out.println("Error al crear el lloguer");
	    }
	}
	
	
	@Test
    public void SenseLloguers() {
		String informeComprova = "Informe de lloguers del client Victor (41007341R)\n";
		informeComprova= informeComprova + "Import a pagar: 0.0€\n";
		informeComprova= informeComprova + "Punts guanyats: 0";
		Client client = new Client("41007341R", "Victor", "691308569");
		assertEquals(informeComprova, client.informe());
	}
		
	//main dels Tests
    public static void main(String args[]) {
        org.junit.runner.JUnitCore.main("TestLloguer");
    }
}
