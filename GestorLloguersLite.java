import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

@SuppressWarnings("unused")
public class GestorLloguersLite {
	
	public static String informeLloguers(Client c){
		int count = 1;
		Vector<Lloguer> lloguers = c.getLloguers();
		String result;
		
		result = "Client: " + c.getNom() + "\n\t" + c.getNif() + "\n\t" + c.getTelefon() + "\nLloguers:" + c.getLloguers().size();
		
		for (Lloguer lloguer : lloguers){
			result = result + "\n\t" + count + ".vehicle: " + lloguer.getVehicle().getMarca() + " " + lloguer.getVehicle().getModel();
			result = result + "\n\t data d'inici: " + lloguer.getData();
			result = result + "\n\t dies llogats: " + lloguer.getDies();
			count++;
		}
		
		return result;	
	}
	

	public static void main (String args[]) {
		Client client = new Client("41007341R","Victor","691308569");

	    Vehicle vehicleBasic = new Vehicle("Leon", "Seat", Vehicle.BASIC);
	    Vehicle vehicleGeneral = new Vehicle("Benz", "Mercedes", Vehicle.GENERAL);
	    Vehicle vehicleLuxe = new Vehicle("Mustang", "Ford", Vehicle.LUXE);
	    

	    SimpleDateFormat format = new SimpleDateFormat("d/M/yyyy");
	    
	    try {
	    Date date = format.parse("2/5/2016");

			Lloguer lloguerBasic = new Lloguer(date, 4, vehicleBasic);
			Lloguer lloguerGeneral = new Lloguer(date, 7, vehicleGeneral);
			Lloguer lloguerLuxe = new Lloguer(date, 7, vehicleLuxe);
			
			//client.afegeix(lloguerBasic);
			//client.afegeix(lloguerGeneral);
			client.afegeix(lloguerLuxe);

		
	    } catch ( Exception e){
	    	System.out.println("Error al crear el lloguer");
	    }
	    
	    //System.out.println(informeLloguers(client));
	    System.out.println(client.informeHTML());
	}
}
